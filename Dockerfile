FROM node

MAINTAINER Jonathan DUPUICH

RUN apt-get update && apt-get install -y vim mongodb
WORKDIR /usr/src/app
RUN git clone https://github.com/brantje/webspy.git /usr/src/app
RUN npm install

EXPOSE 8082
CMD mongod --config /etc/mongodb.conf --smallfiles & node app
