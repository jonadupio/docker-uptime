# docker-uptime

[![build status](https://gitlab.com/jonadupio/docker-uptime/badges/master/build.svg)](https://gitlab.com/jonadupio/docker-uptime/commits/master)

This Docker image provide a pingdom-like solution to monitor all your http/https
services. 

This image can run by itself, without docker-compose.
Mongo is installed in the image, and run in background.

Only for testing, not for production.

## Run the container

```docker run -d -p 80:8082 registry.gitlab.com/jonadupio/docker-uptime```